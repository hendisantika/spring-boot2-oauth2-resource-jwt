package com.hendisantika.springboot2oauth2resourcejwt.controller;

import com.hendisantika.springboot2oauth2resourcejwt.model.CustomPrincipal;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot2-oauth2-resource-jwt
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 29/12/19
 * Time: 16.59
 */
@RestController
public class ResourceController {

    @GetMapping("/context")
    @PreAuthorize("hasAuthority('role_admin')")
    public String context() {
        CustomPrincipal principal = (CustomPrincipal) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal();
        return principal.getUsername() + " " + principal.getEmail();
    }

    @GetMapping("/secured")
    @PreAuthorize("hasAuthority('role_admin')")
    public String secured(CustomPrincipal principal) {
        return principal.getUsername() + " " + principal.getEmail();
    }

    @GetMapping("/resource")
    public String resource() {
        return UUID.randomUUID().toString();
    }
}
