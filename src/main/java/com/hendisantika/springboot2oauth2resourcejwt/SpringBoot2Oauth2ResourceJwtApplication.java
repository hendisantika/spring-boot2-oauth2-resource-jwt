package com.hendisantika.springboot2oauth2resourcejwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot2Oauth2ResourceJwtApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBoot2Oauth2ResourceJwtApplication.class, args);
    }

}
